# Front

------------

><img src="https://i.pinimg.com/280x280_RS/6b/68/be/6b68bed191fdd2fad36e4193e64764ee.jpg" width="50" height="50" alt="Carlin Fongang">

>Carlin FONGANG | fongangcarlin@gmail.com

>[LinkedIn](https://www.linkedin.com/in/carlinfongang/) | [GitLab](https://gitlab.com/carlinfongang) | [GitHub](https://github.com/carlinfongang) | [Credly](https://www.credly.com/users/carlin-fongang/badges)

_______

# Préparation de l'environnement de test

## Préparer un noeud Jenkins (env linux) pour le traitement des jobs

### [1. Provisionner une instance ec2 avec terraform](https://gitlab.com/CarlinFongang-Labs/Terraform/hub/terraform-provide-ec2-instance#remote-state-avec-gitlab)

### [2. Configurer un agent Jenkins (slaves)](https://gitlab.com/CarlinFongang-Labs/projet-odoo/deploy-odoo-postgresql-pgadmin/jenkins_ansible-cicd-for-odoo-and-pgadmin#322-configuration-de-lagent-jenkins-slaves)




## Installation de Compose

### 1. Mettre à jour le gestionnaire de paquets :
```bash
sudo apt update
```
### 2. Installer les dépendances requises :

```bash
sudo apt install -y php-cli unzip
```

### 3. Télécharger l'installateur de Composer :

```bash
php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"
```

### 4. Vérifier l'authenticité du script d'installation :

```bash
php -r "if (hash_file('sha384', 'composer-setup.php') === 'dac665fdc30fdd8ec78b38b9800061b4150413ff2e3b6f88543c636f7cd84f6db9189d43a81e5503cda447da73c7e5b6') { echo 'Installer verified'; } else { echo 'Installer corrupt'; unlink('composer-setup.php'); } echo PHP_EOL;"
```

### 5. Installer Composer globalement :

```bash
php composer-setup.php --install-dir=/usr/local/bin --filename=composer
```
ou

```bash
sudo apt install -y composer
```

### 6. Supprimer le script d'installation :

```bash
php -r "unlink('composer-setup.php');"
```

### 7. Installer flex

Installer l'extension XML pour PHP

```bash
sudo apt install -y php-xml
```

Vérifier l'installation de l'extension XML

```bash
php -m | grep xml
```

```bash
composer require symfony/flex
```

### Vérification de l'installation
```bash
composer --version
```

## Build et exécution des conteneur Docker

### 7. Cloner le dépôt du projet


```bash
git clone https://[USERNAME_GITHUB]:<your_personal_access_token>@github.com/[USERNAME]/[reponame].git
```

### 8. Cloner le dépôt symfony-docker

```bash
git clone https://github.com/dunglas/symfony-docker.git
```

### 9. Copier le template symfony docker dans le repertoire du projet

```bash
cp -Rp symfony-docker/. my_project_path
```
![Alt text](image-4.png)


### 7. Activez le support Docker de Symfony Flex

```bash
cd project_path
```

```bash
composer config --json extra.symfony.docker 'true'
```
### 8. Réexécutez les recettes pour mettre à jour les fichiers liés à Docker en fonction des packages que vous utilisez

```bash
rm ../symfony.lock 
composer recipes:install --force --verbose
```

![Alt text](image.png)

```bash
git diff
```
![Alt text](image-1.png)

### 9. Build de l'image d'ocker

```bash
docker compose build --no-cache --pull
```

![Alt text](image-2.png)
*Build du front*

### Démarrage du projet
```bash
docker compose up -d
```

![Alt text](image-3.png)