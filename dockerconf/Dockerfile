#syntax=docker/dockerfile:1.4

# Versions
FROM php:8.1-apache

# PHP extensions in official PHP Docker images
ADD --chmod=0755 https://github.com/mlocati/docker-php-extension-installer/releases/latest/download/install-php-extensions /usr/local/bin/

RUN install-php-extensions pdo_pgsql intl zip

# Installation de Composer
RUN curl -sSk https://getcomposer.org/installer | php -- --disable-tls && \
    mv composer.phar /usr/local/bin/composer

# NodeJS et NPM
RUN apt update && apt install -yqq nodejs npm unzip

# Ajout d'un utilisateur non-root pour exécuter les commandes
RUN useradd -m -u 1000 dockeruser

# Copy du contenu de l'application ADF Front dans l'image Docker
COPY . /var/www/

# Change ownership of the application files
RUN chown -R dockeruser:dockeruser /var/www/

# Copy du fichier de conf personnalisé apache pour symfony
COPY ./docker/000-default.conf /etc/apache2/sites-available/000-default.conf

# Activer la configuration Apache
RUN a2ensite 000-default

# Définir les variables d'environnement nécessaires
ENV APP_ENABLE_PAYMENTS=1 \
    APP_ENABLE_PAYPAL=1 \
    APP_ENABLE_CB=1 \
    APP_ENABLE_OM=1 \
    APP_ENABLE_MTN=1 \
    APP_ENABLE_SEPA=0


# Exécution des cmd pour le composer, npm run build ...
USER dockeruser
WORKDIR /var/www
RUN composer install && \
    npm install --force && \
    npm run build

# Reglage du repertoire par defaut www dans le conteneur
WORKDIR /var/www/

# Exposition du conteneur
EXPOSE 80